<?php

/**
 * @file
 * Provides an additional config form for theme settings.
 */

use Drupal\Core\Form\FormStateInterface;

function yg_black_form_system_theme_settings_alter(array &$form, FormStateInterface $form_state) {
  $form['visibility'] = [
    '#type' => 'vertical_tabs',
    '#title' => t('YG Black Settings'),
    '#weight' => -999,
  ];

  $form['social']= [
    '#type' => 'details',
    '#title' => t('Social Links'),
    '#weight' => 0,
    '#group' => 'visibility',
    '#open' => FALSE,
  ];
#social links    
  $form['social']['social_links'] = [
    '#type' => 'details',
    '#title' => t('Social Links'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['social']['social_links']['social_title'] = [
    '#type' => 'textfield',
    '#title' => t('Tilte'),
    '#description' => t('Please enter your social title'),
    '#default_value' => theme_get_setting('social_title'),
  ];
  $form['social']['social_links']['facebook_url'] = [
    '#type' => 'textfield',
    '#title' => t('Facebook'),
    '#description' => t('Please enter your facebook url'),
    '#default_value' => theme_get_setting('facebook_url'),
  ];
  $form['social']['social_links']['twitter_url'] = [
    '#type' => 'textfield',
    '#title' => t('Twitter'),
    '#description' => t('Please enter your twitter url'),
    '#default_value' => theme_get_setting('twitter_url'),
  ]; 
  $form['social']['social_links']['dribbble_url'] = [
    '#type' => 'textfield',
    '#title' => t('Dribbble'),
    '#description' => t('Please enter your dribbble url'),
    '#default_value' => theme_get_setting('dribbble_url'),
  ];
  $form['social']['social_links']['github_url'] = [
    '#type' => 'textfield',
    '#title' => t('Github'),
    '#description' => t('Please enter your github url'),
    '#default_value' => theme_get_setting('github_url'),
  ];

  // About-us
  $form['footer']= [
    '#type' => 'details',
    '#title' => t('Footer'),
    '#weight' => 2,
    '#group' => 'visibility',
    '#open' => FALSE,
  ];
  $form['footer']['about_us'] = [
    '#type' => 'details',
    '#title' => t('About Us'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['footer']['about_us']['about_us_title'] = [
    '#type' => 'textfield',
    '#title' => t('About Us Title'),
    '#description' => t('Please enter about-us title'),
    '#default_value' => theme_get_setting('about_us_title'),
  ];
  $form['footer']['about_us']['about_desc'] = [
    '#type' => 'textarea',
    '#title' => t('About Description'),
    '#description' => t('Please enter footer about-description'),
    '#default_value' => theme_get_setting('about_desc'),
  ];

  // Contact-us
  $form['footer']['contact_us'] = [
    '#type' => 'details',
    '#title' => t('Contact Us'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['footer']['contact_us']['title'] = [
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('Please enter contact-us title'),
    '#default_value' => theme_get_setting('title'),
  ];
  $form['footer']['contact_us']['address'] = [
    '#type' => 'textarea',
    '#title' => t('Address'),
    '#description' => t('Please enter address'),
    '#default_value' => theme_get_setting('address'),
  ];
  $form['footer']['contact_us']['contact_number'] = [
    '#type' => 'textfield',
    '#title' => t('Contact Number'),
    '#description' => t('Please enter contact-number'),
    '#default_value' => theme_get_setting('contact_number'),
  ];
  $form['footer']['contact_us']['mail'] = [
    '#type' => 'textfield',
    '#title' => t('Email Id'),
    '#description' => t('Please enter contact mail-id'),
    '#default_value' => theme_get_setting('mail'),
  ];
  $form['footer']['contact_us']['domain_name'] = [
    '#type' => 'textfield',
    '#title' => t('Domain Name'),
    '#description' => t('Please enter domain name'),
    '#default_value' => theme_get_setting('domain_name'),
  ];
}
